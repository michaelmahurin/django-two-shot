from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from receipts.form import ReceiptForm

# Create your views here.


@login_required
def receipt_list(request):
    receiptlist = Receipt.objects.filter(purchaser =request.user)
    context = {"receiptlist": receiptlist}
    return render(request, "receipts/list.html", context)



@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)
